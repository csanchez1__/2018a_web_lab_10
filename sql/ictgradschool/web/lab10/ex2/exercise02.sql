-- Answers to Exercise 2 here
DROP TABLE IF EXISTS ex2_JSONtable;

CREATE TABLE IF NOT EXISTS ex2_JSONtable(
    username VARCHAR(60),
    first_name VARCHAR(60),
    last_name VARCHAR(60),
    email VARCHAR(60)
);

INSERT INTO ex2_JSONtable VALUES('programmer1', 'Bill', 'Gates', 'bill@microsoft.com');
INSERT INTO ex2_JSONtable (username, first_name, last_name, email) VALUES
    ('programmer2', 'Peter', 'Williams', 'peter@microsoft.com'),
    ('networkadmin1', 'Pete', 'Jones', 'pete@microsoft.com'),
    ('database1', 'Harry', 'Peterson', 'harry@microsoft.com');
