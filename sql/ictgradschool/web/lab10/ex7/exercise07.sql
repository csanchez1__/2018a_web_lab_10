-- Answers to Exercise 7 here
DROP TABLE IF EXISTS ex7_articleComments;

CREATE TABLE IF NOT EXISTS ex7_articleComments(
    commentId INT(40) NOT NULL AUTO_INCREMENT,
    comments VARCHAR(150),
    PRIMARY KEY (commentId),
    FOREIGN KEY (commentId) REFERENCES ex4_article(id)
);

INSERT INTO ex7_articleComments (comments) VALUES
    ('This article is horrible'),
    ('This article is informative'),
    ('Amazing article'),
    ('Good enough');
