-- Answers to Exercise 8 here
DELETE FROM ex5_JSONtable WHERE username LIKE 'database1';
ALTER TABLE ex5_JSONtable DROP COLUMN last_name;
DROP TABLE ex5_JSONtable;

UPDATE ex6_movie SET director = 'Vas Vasquez' WHERE movieName LIKE '2012';
UPDATE ex6_movie SET weeklyCharge = 6 WHERE movieName LIKE 'Sword Art';

UPDATE ex7_articleComments SET commentId = 5 WHERE ex7_articleComments.comments LIKE 'Amazing%';
