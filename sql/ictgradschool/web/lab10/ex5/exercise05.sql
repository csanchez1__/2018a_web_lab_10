-- Answers to Exercise 5 here
DROP TABLE IF EXISTS ex5_JSONtable;

CREATE TABLE IF NOT EXISTS ex5_JSONtable(
    username VARCHAR(60) NOT NULL UNIQUE,
    first_name VARCHAR(60),
    last_name VARCHAR(60),
    email VARCHAR(60),
    PRIMARY KEY (username)
);

INSERT INTO ex5_JSONtable VALUES('programmer1', 'Bill', 'Gates', 'bill@microsoft.com');
INSERT INTO ex5_JSONtable (username, first_name, last_name, email) VALUES
    ('programmer2', 'Peter', 'Williams', 'peter@microsoft.com'),
    ('networkadmin1', 'Pete', 'Jones', 'pete@microsoft.com'),
    ('database1', 'Harry', 'Peterson', 'harry@microsoft.com');
INSERT INTO ex5_JSONtable VALUES('programmer1', 'Wayne', 'Barry', 'wayne@microsoft.com');
