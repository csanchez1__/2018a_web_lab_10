-- Answers to Exercise 9 here
SELECT * FROM ex3_videoRental;

SELECT barcodeId, name, gender, year_born, joined FROM ex3_videoRental;

SELECT title FROM ex4_article;

SELECT DISTINCT director FROM ex6_movie;

SELECT movieName FROM ex6_movie WHERE weeklyCharge = 2;

SELECT username FROM ex5_JSONtable ORDER BY username;
SELECT username FROM ex2_JSONtable ORDER BY username;

SELECT username FROM ex5_JSONtable WHERE first_name LIKE 'Pete%';

SELECT username FROM ex5_JSONtable WHERE first_name LIKE 'Pete%'
                                    OR last_name LIKE 'Pete%';
