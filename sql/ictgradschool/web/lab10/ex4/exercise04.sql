-- Answers to Exercise 4 here
DROP TABLE IF EXISTS ex4_article;

CREATE TABLE IF NOT EXISTS ex4_article(
    id INT NOT NULL AUTO_INCREMENT,
    title VARCHAR(20),
    articleText VARCHAR(800),
    PRIMARY KEY (id)
);

INSERT INTO ex4_article (title, articleText) VALUES
    ('Westside News', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec at libero faucibus, congue libero sodales, dignissim nibh. Vivamus eleifend efficitur vehicula. Morbi sed nisl arcu. Suspendisse bibendum dignissim ante vitae facilisis. Mauris gravida vel orci at consectetur. Aliquam ut sem nec mauris suscipit ornare ac non lorem. Ut arcu eros, cursus et mi et, tincidunt ultricies dolor. '),
    ('April Article', 'Integer tempus suscipit vehicula. Mauris dignissim, purus at iaculis sodales, magna augue finibus nisi, nec fermentum risus ipsum at orci. Vestibulum mi nisl, tempus posuere ipsum nec, cursus pellentesque neque. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam sodales neque id tellus vehicula, eu congue est varius. Mauris sit amet tortor et odio vulputate consequat. '),
    ('Game informer', 'Curabitur rhoncus porttitor tortor sed ornare. Nullam imperdiet sapien at purus convallis volutpat. Nunc pharetra condimentum ipsum, eu blandit justo rutrum a. Sed a ante mi. Duis mattis nisl nec gravida vulputate. Nunc porta dui mi, varius ultrices nibh mollis ut. Integer commodo erat lorem, ac semper ante egestas in. Aenean nunc lectus, mattis a imperdiet a, venenatis sed libero. '),
    ('International Daily', 'Aliquam sed blandit odio. Pellentesque vel mauris euismod metus volutpat efficitur. Donec erat elit, sollicitudin vitae metus vitae, scelerisque elementum orci. Etiam ultricies pulvinar convallis. Aenean orci risus, placerat ut nisi in, suscipit sagittis metus. '),
    ('Back Alley News', 'Duis ut rhoncus diam. Sed sit amet sem a justo pulvinar vulputate. Maecenas sagittis tincidunt arcu, id euismod augue aliquam scelerisque. Mauris semper auctor porttitor. Duis pretium aliquam mattis. Ut mollis tristique tellus, eget ullamcorper nisi blandit eu. ');
