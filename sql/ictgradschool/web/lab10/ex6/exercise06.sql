-- Answers to Exercise 6 here
ALTER TABLE ex3_videoRental ADD barcodeId INT(40) PRIMARY KEY AUTO_INCREMENT;

DROP TABLE IF EXISTS ex6_movie;

CREATE TABLE IF NOT EXISTS ex6_movie(
    movieId INT(40) NOT NULL AUTO_INCREMENT,
    movieName VARCHAR(15),
    director VARCHAR(20),
    weeklyCharge INT(4),
    PRIMARY KEY (movieId),
    FOREIGN KEY (movieId) REFERENCES ex3_videoRental(barcodeId)
);

INSERT INTO ex6_movie (movieName, director, weeklyCharge) VALUES
    ( '2012', 'John Doe', 2),
    ( 'Jumanji', 'Felix Patt', 4),
    ( 'Player One', 'Sammy Lane', 6),
    ( '300 BC', 'John Doe', 4),
    ( 'Power Rangers', 'Sammy Lane', 6),
    ( 'Sword Art', 'JJ Adams', 2);
